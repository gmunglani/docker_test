import snowflake.connector
import pandas as pd
import pickle
import os
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
from datetime import datetime


def snowflake_connector():
    ###connect to snowflake directly
    conn = snowflake.connector.connect(
                user='GAUTAMMUNGLANI',
                host='wn87522.west-europe.azure.snowflakecomputing.com',
                password='AF67HLi68jCzLRU',
                account='wn87522',
                warehouse='GENERAL_WH',
                database='CONSUME',
                session_parameters={'QUERY_TAG':'AzureTest'}
                )
    
    return conn

def snowflake_engine():
    ##alternative way of creating a connection to snowflake using an engine
    engine = create_engine(URL(
        account = 'wn87522',
        user = 'GAUTAMMUNGLANI',
        host='wn87522.west-europe.azure.snowflakecomputing.com',
        password = 'AF67HLi68jCzLRU',
        database = 'sandbox',
        schema = 'pricing2021',
        warehouse = 'general_wh',
        role='proxinea',
        numpy=True
    ))

    return engine

def upload_to_snowflake(data_frame, engine, table_name, truncate=True, create=False):
    dt = datetime.now()
    #file_path = "/home/gm/Documents/Projects/docker_azure_integration/"
    file_path = "/usr/app/"
    timeStampExtension = dt.strftime("%Y%m%d%H%M%S%f")
    file_name = f"{file_path}{table_name}_{timeStampExtension}.csv"
    data_frame.to_csv(file_name, index=False, header=False)

    with engine.connect() as con:

        if create:
            data_frame.head(0).to_sql(name=table_name, 
                                      con=con, 
                                      index=False)
        if truncate:
            con.execute(f"truncate table {table_name}")
      
 #       data_frame.to_sql(table_name,con=con,index=False)

        con.execute(f"put file://{file_name} @%{table_name}")
        con.execute(f"copy into {table_name}") 

    os.remove(file_name)


def read_data(conn,sql):
    df = pd.read_sql_query(con=conn, sql=sql)
    df = df.dropna()

    return df

conn = snowflake_connector()
sql = 'SELECT * FROM "SANDBOX"."PRICING2021".tmp_fre_elasticity_basis_2'
df = read_data(conn, sql)
dfn = df.iloc[11:20,:]

engine = snowflake_engine()
upload_to_snowflake(dfn, engine, 'TMP_GMU_AZURE_TEST', truncate = True, create = False)
