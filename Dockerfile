# syntax=docker/dockerfile:1

# Use python base image
FROM python:3.9-bullseye

# Set image working directory
WORKDIR /usr/app/

# Copy requirements into image
COPY requirements.txt requirements.txt

# Install dependencies with pip
RUN pip3 install -r requirements.txt

# Copy python scripts into the image
COPY . .

# Run python main file
CMD ["python", "dev.py"]